%% Assess condition differences in 1st derivative of pupil diameter: YA

%% paths

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.fieldtrip        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.shadedError      = [pn.root, 'B_analyses/A_pupilDilation/T_tools/shadedErrorBar/']; addpath(pn.shadedError);
pn.naninterp        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/naninterp/']; addpath(pn.naninterp);
pn.behavior         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
pn.eyeDataEEG       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/';
pn.eyeDataMrk       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/B_EEG_ET_ByRun/';
pn.collectedDataOut = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];
pn.statsOut         = [pn.root, 'B_analyses/A_pupilDilation/B_data/C_stats/'];
pn.dataOut          = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];

%% load results

load([pn.dataOut, 'G1_summaryPupil.mat'], 'summaryPupil')
load([pn.dataOut, 'G1_pupilDataFT.mat'], 'pupilDataFT')

%% retrieve age labels

idxYA = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))<2000;
idxOA = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>2000;
ageIndices{1} = idxYA; ageName{1} = 'YoungAdults';
ageIndices{2} = idxOA; ageName{2} = 'OldAdults';

%% perform statistical testing (CBPA) with FieldTrip: derivative data (unsmoothed)

pupilYA = [];
curIDs = find(idxYA);
for indID = 1:numel(curIDs)
    for indCond = 1:4
        pupilYA{indCond, indID}.dataDeriv = permute(squeeze(nanmean(pupilDataFT{indCond}.dataDeriv(curIDs(indID),:,:),2)),[2,1]);
        pupilYA{indCond, indID}.dimord = 'chan_time';
        pupilYA{indCond, indID}.time = pupilDataFT{indCond}.time;
        pupilYA{indCond, indID}.label{1} = 'pupilDia';
    end
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'dataDeriv';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat] = ft_timelockstatistics(cfgStat, pupilYA{1,:}, pupilYA{2,:}, pupilYA{3,:}, pupilYA{4,:});

figure; imagesc(stat.mask)

% run paired t-tests

% subj = 47;
% conds = 2;
% design = zeros(2,conds*subj);
% for indCond = 1:conds
% for i = 1:subj
%     design(1,(indCond-1)*subj+i) = indCond;
%     design(2,(indCond-1)*subj+i) = i;
% end
% end
% cfgStat.design   = design;
% [stat_12] = ft_timelockstatistics(cfgStat, pupilYA{1,:}, pupilYA{2,:});
% [stat_23] = ft_timelockstatistics(cfgStat, pupilYA{2,:}, pupilYA{3,:});
% [stat_34] = ft_timelockstatistics(cfgStat, pupilYA{3,:}, pupilYA{4,:});

%% multivariateF test

% cfgStat = [];
% cfgStat.method           = 'montecarlo';
% cfgStat.statistic        = 'ft_statfun_depsamplesFmultivariate';
% cfgStat.correctm         = 'cluster';
% cfgStat.clusteralpha     = 0.05;
% cfgStat.clusterstatistic = 'maxsum';
% cfgStat.minnbchan        = 0;
% cfgStat.tail             = 1;
% cfgStat.clustertail      = 1;
% cfgStat.alpha            = 0.05;
% cfgStat.numrandomization = 500;
% cfgStat.parameter        = 'dataDeriv';
% cfgStat.neighbours      = []; % no neighbors here 
% 
% subj = 47;
% conds = 4;
% design = zeros(2,conds*subj);
% for indCond = 1:conds
% for i = 1:subj
%     design(1,(indCond-1)*subj+i) = indCond;
%     design(2,(indCond-1)*subj+i) = i;
% end
% end
% cfgStat.design   = design;
% cfgStat.ivar     = 1;
% cfgStat.uvar     = 2;
% 
% [stat] = ft_timelockstatistics(cfgStat, pupilYA{1,:}, pupilYA{2,:}, pupilYA{3,:}, pupilYA{4,:});
% 
% figure; imagesc(stat.mask)

save([pn.statsOut, 'G1_parametric_1stderiv.mat'], 'cfgStat', 'stat*')
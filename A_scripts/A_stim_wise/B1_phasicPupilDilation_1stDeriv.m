%% Create FieldTrip structure with multiple baselining procedures & perform CBPA

% 1228 is missing 58 trials

%% paths

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.fieldtrip        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.shadedError      = [pn.root, 'B_analyses/A_pupilDilation/T_tools/shadedErrorBar/']; addpath(pn.shadedError);
pn.naninterp        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/naninterp/']; addpath(pn.naninterp);
pn.behavior         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
pn.eyeDataEEG       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/';
pn.eyeDataMrk       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/B_EEG_ET_ByRun/';
pn.collectedDataOut = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];
pn.statsOut         = [pn.root, 'B_analyses/A_pupilDilation/B_data/C_stats/'];
pn.dataOut          = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];

%% load data & create proper data structures with summary measures

load([pn.collectedDataOut, 'B3_pupilData.mat'], 'pupilData')

idxYA = cell2mat(cellfun(@str2num, pupilData.IDs, 'un', 0))<2000;
idxOA = cell2mat(cellfun(@str2num, pupilData.IDs, 'un', 0))>2000;
ageIndices{1} = idxYA; ageName{1} = 'YoungAdults';
ageIndices{2} = idxOA; ageName{2} = 'OldAdults';

% input data are 1000 Hz

% put into fieldtrip structure by condition
% split into 1st and 2nd level?

% .data : raw format data
% .dataZ: z-score normalization across all time points & conditions within run
% .dataRelChange: relative change to baseline: 2.8:3 s 
% .datablock: data rearranged into 8*(8 trial) blocks
% .dataRelChangeBlock: (average across blocks), relative change to baseline: 2.8:3 s 

pupilDataFT = [];
for indCond = 1:4
    Nsubs = size(pupilData.dataStim,1);
    Ntime = size(pupilData.dataStim,3)/5;
    Ntrials = 64;
    pupilDataFT{indCond}.data = NaN(Nsubs, Ntrials, Ntime);
    Nsubs = size(pupilData.dataStim,1);
    for indID = 1:Nsubs
        condTrials = pupilData.TrlInfo.StateOrders(indID,:)==indCond;
        tmpData = squeeze(pupilData.dataStim(indID,condTrials,:));
        % downsample to 200 Hz
        pupilDataFT{indCond}.data(indID,1:size(find(condTrials),2),:) = ft_preproc_resample(tmpData, 1000, 200, 'resample');
    end
    pupilDataFT{indCond}.dimord = 'subj_rpt_time';
    pupilDataFT{indCond}.time = pupilData.time(1:5:end);
    pupilDataFT{indCond}.Fs = 200;
    pupilDataFT{indCond}.descriptor = ['Pupil data for load ', num2str(indCond)];
    % create first derivative
    derivData = diff(pupilDataFT{indCond}.data(:,:,:),1,3);
    Bltime = find(pupilDataFT{indCond}.time > -.5 & pupilDataFT{indCond}.time < 0);
    meanBL = nanmean(derivData(:,:,Bltime+1),3);
    meanBL = repmat(meanBL, 1,1,Ntime); % trial-wise baseline
    pupilDataFT{indCond}.dataDeriv = derivData;%(pupilDataFT{indCond}.data-meanBL)./meanBL;
    % smooth the first derivative
    tmpSmooth = smoothdata(derivData,3,'movmedian', 40); % 200 Hz input data, 200 ms smoothing
    pupilDataFT{indCond}.dataDerivSmooth = tmpSmooth;
end
IDs = pupilData.IDs;

%% create summary measures

summaryPupil = []; tmp_stimTime = find(pupilDataFT{1}.time>3 & pupilDataFT{1}.time<4.5);
for indCond = 1:4
    summaryPupil.stimPupilDeriv(:,indCond) = ...
        squeeze(nanmean(nanmedian(pupilDataFT{indCond}.dataDeriv(:,:,tmp_stimTime),3),2));
    summaryPupil.IDs = pupilData.IDs;
end

% calculate linear change in all measures

X = [1,1; 1,2; 1,3; 1,4];
b=X\summaryPupil.stimPupilDeriv'; % matrix implementation of multiple regression        
summaryPupil.stimPupilDeriv_slopes = b(2,:);

%% save results

save([pn.dataOut, 'G1_summaryPupil.mat'], 'summaryPupil', 'IDs')
save([pn.dataOut, 'G1_pupilDataFT.mat'], 'pupilDataFT')

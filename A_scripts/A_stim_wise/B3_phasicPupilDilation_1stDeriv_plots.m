%% PLOT results for 1st derivative of pupil diameter: YA

%% paths

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/';
pn.fieldtrip        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.shadedError      = [pn.root, 'B_analyses/A_pupilDilation/T_tools/shadedErrorBar/']; addpath(pn.shadedError);
pn.naninterp        = [pn.root, 'B_analyses/A_pupilDilation/T_tools/naninterp/']; addpath(pn.naninterp);
pn.behavior         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/';
pn.eyeDataEEG       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/';
pn.eyeDataMrk       = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/A_preproc/SA_preproc_study/B_data/B_EEG_ET_ByRun/';
pn.collectedDataOut = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];
pn.statsOut         = [pn.root, 'B_analyses/A_pupilDilation/B_data/C_stats/'];
pn.dataOut          = [pn.root, 'B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/'];

%% load results

load([pn.dataOut, 'G1_summaryPupil.mat'], 'summaryPupil')
load([pn.dataOut, 'G1_pupilDataFT.mat'], 'pupilDataFT')

%% retrieve age labels

idxYA = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))<2000;
idxOA = cell2mat(cellfun(@str2num, summaryPupil.IDs, 'un', 0))>2000;
ageIndices{1} = idxYA; ageName{1} = 'YoungAdults';
ageIndices{2} = idxOA; ageName{2} = 'OldAdults';

%% load statistical results for baselined data

load([pn.statsOut, 'G1_parametric_1stderiv.mat'])

%% plot temporal traces with statistics

catAll = cat(4,pupilDataFT{2}.dataDerivSmooth, pupilDataFT{3}.dataDerivSmooth,...
        pupilDataFT{4}.dataDerivSmooth);

time = pupilDataFT{1}.time;
time = time(2:end)-3;

h = figure('units','normalized','position',[.1 .1 .25 .3]);
cla; hold on; 
% highlight different experimental phases in background
patches.timeVec = [3 6]-3;
patches.colorVec = [1 .95 .8];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [-.25 1];
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
condAvg = squeeze(nanmean(nanmean(catAll(ageIndices{1},:,:,:),2),4))-squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(ageIndices{1},:,:),2));

curData = squeeze(nanmean(pupilDataFT{2}.dataDerivSmooth(ageIndices{1},:,:),2))-squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(ageIndices{1},:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{1})),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

curData = squeeze(nanmean(pupilDataFT{3}.dataDerivSmooth(ageIndices{1},:,:),2))-squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(ageIndices{1},:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{1})),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

curData = squeeze(nanmean(pupilDataFT{4}.dataDerivSmooth(ageIndices{1},:,:),2))-squeeze(nanmean(pupilDataFT{1}.dataDerivSmooth(ageIndices{1},:,:),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),numel(find(ageIndices{1})),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
legend([l2.mainLine, l3.mainLine, l4.mainLine], {'L2-L1'; 'L3-L1'; 'L4-L1'}, 'orientation', 'horizontal', 'location', 'South'); legend('boxoff');

xlim([2.75 6.25]-3);  ylim([-.25 1])
ylabel({'Pupil diameter change';'(1st derivative)'}); xlabel('Time (s from stim onset)')
title({'Pupil diameter during stimulus increases with load'; ''})
set(findall(gcf,'-property','FontSize'),'FontSize',18)

sigVals = double(stat.mask);
sigVals(sigVals==0) = NaN;
hold on; plot(time, sigVals.*-.025, 'k', 'linewidth', 10)

% TO DO: add statistical results

% plot as inset: overall pupil dilation

handaxes2 = axes('Position', [0.56 0.6 0.25 .25]);

    cla; hold on; 
    % highlight different experimental phases in background
    patches.timeVec = [3 6]-3;
    patches.colorVec = [.9 .9 .9];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-14 4];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    hold on; plot(time,squeeze(nanmean(nanmean(pupilDataFT{1}.dataDerivSmooth(ageIndices{1},:,:),2),1)), 'k', 'LineWidth', 2);
    ylabel({'Pupil diameter change';'(1st derivative)'}); xlabel('Time (s from stim onset)')
    title('Pupil diameter change: L1')
    xlim([2.75 6.25]-3); ylim([-15 5]);

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/C_figures/B_pupilAtStim/';
figureName = 'G1_PupilDerivative_StimOnset';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% summary pupil as median within cluster

% get the median only of significant time points

tmp_stimTime = find(pupilDataFT{1}.time>3 & pupilDataFT{1}.time<4.5);
%tmp_stimTime = tmp_stimTime(1) + find(sigVals(tmp_stimTime) == 1);
%summaryPupil = [];
for indCond = 1:4
    summaryPupil.stimPupilDeriv(:,indCond) = ...
        squeeze(nanmean(nanmedian(pupilDataFT{indCond}.dataDeriv(:,:,tmp_stimTime),3),2));
    summaryPupil.IDs = summaryPupil.IDs;
end

% calculate linear change in all measures

X = [1,1; 1,2; 1,3; 1,4];
b=X\summaryPupil.stimPupilDeriv'; % matrix implementation of multiple regression        
summaryPupil.stimPupilDeriv_slopes = b(2,:);

%% correlate with PLS factor

% correlate with PLS factor

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/M2_mencemtPLS_v6_wGamma.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

indLV = 1;

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
    end
end

BS_meancent = uData{indGroup};

h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    a = squeeze(nanmean(BS_meancent(2:4,:),1))-BS_meancent(1,:); a =a';
    %b = nanmean(summaryPupil.stimPupilDeriv(1:47,2:4),2)-summaryPupil.stimPupilDeriv(1:47,1);
    b = summaryPupil.stimPupilDeriv_slopes(1:47)';
    %b = nanmean(summaryPupil.stimPupilRelChange(1:47,4),2)-summaryPupil.stimPupilRelChange(1:47,1);
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Brainscore (234-1)'); ylabel('Linear pupil dilation (1st deriv.) effect');
    title({'Multivariate spectral changes inter-individually';'relate to pupil dilation effect'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/C_figures/B_pupilAtStim/';
figureName = 'G1_PupilDeriv_BrainScore';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% assess linearity of effect

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/RainCloudPlots'))

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = summaryPupil.stimPupilDeriv(1:47,i);
        data_ws{i, j} = summaryPupil.stimPupilDeriv(1:47,i)-...
            nanmean(summaryPupil.stimPupilDeriv(1:47,:),2)+...
            repmat(nanmean(nanmean(summaryPupil.stimPupilDeriv(1:47,:),2),1),size(summaryPupil.stimPupilDeriv(1:47,:),1),1); % individually demean: within-subject viz
    end
end

cl = [.6 .2 .2];

% make figure
h = figure('units','normalized','position',[.1 .1 .15 .2]); cla;
h_rc = rm_raincloud(data_ws, cl,1);
% add confidence intervals
hold on;
% for indCond = 1:4
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData + errorY{indGroup}(1,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
%     line([h_rc.m(indCond,1).XData, h_rc.m(indCond,1).XData - errorY{indGroup}(2,indCond)],[h_rc.m(indCond,1).YData,h_rc.m(indCond,1).YData], 'Color', 'k', 'LineWidth', 5)
% end

% add stats
% condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
% condPairsLevel = [-1.5 -1.4 -1.3 -1.2 -1.1, -1.0];
condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [-1.5 -1.4 -1.3];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(data{condPairs(indPair,1)}, data{condPairs(indPair,2)}); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
       mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);
    end
end

view([90 -90]);
axis ij

set(gca, 'YTickLabels', flip({'1'; '2'; '3'; '4'})); % label assignment also has to be flipped
ylabel('Target load')
xlabel({'Pupil (1st derivative)';'[individually centered]'})
xlim([-4.3 -1]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

% test linear effect
X = [1 1; 1 2; 1 3; 1 4]; b=X\summaryPupil.stimPupilDeriv(1:47,:)'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);

title('Linear effect: p = 1.23e-6')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/C_figures/B_pupilAtStim/';
figureName = 'G1_PupilDeriv_RainCloud';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% Assess association with drift rate

% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
% idx_IDs = find(ismember(STSWD_summary.IDs, summaryPupil.IDs(idxYA)));

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vt.mat'], 'HDDM_summary')
idx_IDs = find(ismember(HDDM_summary.IDs, summaryPupil.IDs(idxYA)));

h = figure('units','normalized','position',[.1 .1 .2 .3]);
subplot(1,2,1); cla; hold on;
    a = nanmean(HDDM_summary.driftEEG(idx_IDs,1),2);
    b = summaryPupil.stimPupilDeriv_slopes(idxYA)';
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Drift (L1)'); ylabel('Linear pupil dilation effect');
    title({'Drift rates inter-individually';'relate to pupil dilation effect'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2); cla; hold on;
    a = nanmean(HDDM_summary.driftEEG(idx_IDs,2:4),2)-nanmean(HDDM_summary.driftEEG(idx_IDs,1),2);
    b = summaryPupil.stimPupilDeriv_slopes(idxYA)';
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthEast'); legend('boxoff')
    xlabel('Drift (234-1)'); ylabel('Linear pupil dilation effect');
    title({'Drift rates inter-individually';'relate to pupil dilation effect'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

% assemble table
Subject = repmat(summaryPupil.IDs(idxYA),1,4); Subject = reshape(Subject', 47*4,1);
Condition = repmat([1:4]',47,1);
Pupil = reshape(summaryPupil.stimPupilDeriv(idxYA,1:4)', 47*4,1);
Drift = reshape(HDDM_summary.driftEEG(idx_IDs,1:4), 47*4,1);

tbl = table(double(Condition),double(Pupil),double(Drift),Subject,'VariableNames',{'Condition','Pupil','Drift', 'Subject'});
lme = fitlme(tbl,'Drift~Pupil+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

%% correlate with behavior

figure; scatter(STSWD_summary.HDDM_vt.driftEEG_linear(idx_AttFactor,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1), 'filled')
    [r, p] = corrcoef(STSWD_summary.HDDM_vt.driftEEG_linear(idx_AttFactor,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1));

% r =
% 
%     1.0000    0.3124
%     0.3124    1.0000
% 
% 
% p =
% 
%     1.0000    0.0325
%     0.0325    1.0000
    
figure; scatter(STSWD_summary.HDDM_vt.driftEEG(idx_AttFactor,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1), 'filled')
    [r, p] = corrcoef(STSWD_summary.HDDM_vt.driftEEG(idx_AttFactor,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1));

    
% r =
% 
%     1.0000   -0.2878
%    -0.2878    1.0000
% 
% 
% p =
% 
%     1.0000    0.0498
%     0.0498    1.0000
